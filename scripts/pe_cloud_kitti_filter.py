#!/usr/bin/env python

# Copyright 2021(R) Perception Engine. All rights reserved

from __future__ import print_function
import rospy
import numpy as np

from sensor_msgs.msg import PointCloud2
import ros_numpy
import roslib

roslib.load_manifest('pe_cloud_kitti_filter')


class PeCloudKittiFilter:
    def __init__(self):
        rospy.init_node("pe_cloud_kitti_filter", anonymous=True)

        self.__input_topic = rospy.get_param('~input_topic', 'points_raw')

        self.__filter_x = rospy.get_param('~filter_x', 3.0)
        self.__filter_y = rospy.get_param('~filter_y', 80)
        self.__filter_z = rospy.get_param('~filter_z', 80)
        rospy.loginfo("Filtering Cloud (x,y,z) (:,:,:)".format(self.__filter_x, self.__filter_y, self.__filter_z))

        rospy.loginfo("Subscribed to: {} [sensor_msgs/PointCloud2]".format(self.__input_topic))
        rospy.Subscriber(self.__input_topic, PointCloud2, self.__cloud_callback)

        rospy.loginfo("Publishing to: {} [sensor_msgs/PointCloud2]".format("points_filtered"))
        self.__cloud_lane_pub = rospy.Publisher("/points_forwarded", PointCloud2, queue_size=1)

        rospy.spin()

    def cloud_to_np_indexed_array_intensity(self, cloud):
        xyzi_cloud = np.zeros((cloud.shape[0], 4), dtype=np.float32)
        xyzi_cloud[:, 0] = cloud['x']
        xyzi_cloud[:, 1] = cloud['y']
        xyzi_cloud[:, 2] = cloud['z']
        xyzi_cloud[:, 3] = cloud['intensity']

        return xyzi_cloud

    def convert_cloud_numpy_to_ros_xyzi(self, np_cloud):
        cloud_size = len(np_cloud)
        np_cloud_xyzi = np.zeros((cloud_size,), dtype=[
            ('x', np.float32),
            ('y', np.float32),
            ('z', np.float32),
            ('intensity', np.float32)])

        np_cloud_xyzi['x'] = np_cloud[:, 0]
        np_cloud_xyzi['y'] = np_cloud[:, 1]
        np_cloud_xyzi['z'] = np_cloud[:, 2]
        np_cloud_xyzi['intensity'] = np_cloud[:, 3]

        return np_cloud_xyzi

    def publish_np_cloud(self, publisher, np_cloud, header):
        out_cloud_msg = ros_numpy.msgify(PointCloud2, np_cloud)
        out_cloud_msg.header = header

        publisher.publish(out_cloud_msg)

    def __cloud_callback(self, cloud_msg):
        tmp = ros_numpy.numpify(cloud_msg)

        cloud_size = len(tmp)
        np_cloud_xyzi = np.zeros((cloud_size,), dtype=[
            ('x', np.float32),
            ('y', np.float32),
            ('z', np.float32),
            ('intensity', np.float32)])

        np_cloud_xyzi['x'] = tmp['x']
        np_cloud_xyzi['y'] = tmp['y']
        np_cloud_xyzi['z'] = tmp['z']
        np_cloud_xyzi['intensity'] = tmp['intensity']/np.max(tmp['intensity'])  # Normalize intensity

        np_cloud_xyzi = np_cloud_xyzi[np_cloud_xyzi['z'] < self.__filter_z]
        np_cloud_xyzi = np_cloud_xyzi[np.abs(np_cloud_xyzi['x']) <= self.__filter_x]
        np_cloud_xyzi = np_cloud_xyzi[np.abs(np_cloud_xyzi['y']) <= self.__filter_y]

        cloud = self.cloud_to_np_indexed_array_intensity(np_cloud_xyzi)

        cloud.tofile(str(cloud_msg.header.seq)+'.bin')
        labels = np.zeros((cloud.shape[0],), dtype=np.uint32)  # create empty labels file Randlanet
        labels.tofile(str(cloud_msg.header.seq) + '.label')
        rospy.loginfo_throttle(1, "Saving files in current working directory...")

        tmp_cloud = self.convert_cloud_numpy_to_ros_xyzi(np.array(cloud))
        self.publish_np_cloud(self.__cloud_lane_pub, tmp_cloud, cloud_msg.header)


if __name__ == "__main__":
    filter_node = PeCloudKittiFilter()
